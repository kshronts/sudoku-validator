package kds;

import static org.junit.jupiter.api.Assertions.*;

import java.io.IOException;
import org.junit.jupiter.api.Test;

class SudokuValidatorTest {

  private static final String validFile = "example_valid.csv";
  private static final String invalidFile = "example_invalid.csv";

  private CsvReader csvReader = new CsvReader();
  private SudokuValidator sudokuValidator = new SudokuValidator();

  @Test
  void validate() throws IOException {
    Integer[][] validPuzzle = csvReader.readFile(validFile);

    boolean isValid = sudokuValidator.validate(validPuzzle);
    assertTrue(isValid);
  }

  @Test
  void validate_invalid() throws IOException {
    Integer[][] validPuzzle = csvReader.readFile(invalidFile);

    boolean isValid = sudokuValidator.validate(validPuzzle);
    assertFalse(isValid);
  }

  @Test
  void validateMainGrid() throws IOException {
    Integer[][] validPuzzle = csvReader.readFile(validFile);

    boolean isValid = sudokuValidator.validateMainGrid(validPuzzle);
    assertTrue(isValid);
  }

  @Test
  void validateMainGrid_invalid() throws IOException {
    Integer[][] validPuzzle = csvReader.readFile(invalidFile);

    boolean isValid = sudokuValidator.validateMainGrid(validPuzzle);
    assertFalse(isValid);
  }

  @Test
  void validateSubGrids() throws IOException {
    Integer[][] validPuzzle = csvReader.readFile(validFile);

    boolean isValid = sudokuValidator.validateSubGrids(validPuzzle);
    assertTrue(isValid);

  }

  @Test
  void validateSubGrids_invalid() throws IOException {
    Integer[][] validPuzzle = csvReader.readFile(invalidFile);

    boolean isValid = sudokuValidator.validateSubGrids(validPuzzle);
    assertFalse(isValid);

  }
}