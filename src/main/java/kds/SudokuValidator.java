package kds;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

public class SudokuValidator {

  public static void main(String[] args) {
    SudokuValidator sudokuValidator = new SudokuValidator();
    CsvReader csvReader = new CsvReader();
    try {
      Integer[][] validPuzzle = csvReader.readFile("example_valid.csv");
      Integer[][] invalidPuzzle = csvReader.readFile("example_invalid.csv");

      boolean isValid = sudokuValidator.validate(validPuzzle);
      System.out.println(isValid ? "valid" : "invalid");

      isValid = sudokuValidator.validate(invalidPuzzle);
      System.out.println(isValid ? "valid" : "invalid");

    } catch (IOException e) {
      e.printStackTrace();
    }

  }

  public boolean validate(Integer[][] puzzle) {
    return validateMainGrid(puzzle) && validateSubGrids(puzzle);

  }

  public boolean validateMainGrid(Integer[][] puzzle) {
    Integer[] values = new Integer[9];

    for (int colIdx = 0; colIdx < 9; colIdx++) {

      for (int rowIdx = 0; rowIdx < 9; rowIdx++) {
        //validate the row...
        if (!isSequenceValid(Arrays.stream(puzzle[rowIdx]))) {
          return false;
        }

        //accumulate the col values into an array...
        values[rowIdx] = puzzle[rowIdx][colIdx];
      }

      //validate column
      if (!isSequenceValid(Arrays.stream(values))) {
        return false;
      }

    }

    return true;
  }

//  /**
//   * Validates the 9 sub grids within the puzzle.
//   *
//   * @param puzzle
//   * @return true if valid
//   */
//  private boolean validateSubGrids(Integer[][] puzzle) {
//      return
//          validateSubGrid(0, 0, puzzle) &&
//          validateSubGrid(0, 3, puzzle) &&
//          validateSubGrid(0, 6, puzzle) &&
//          validateSubGrid(3, 0, puzzle) &&
//          validateSubGrid(3, 3, puzzle) &&
//          validateSubGrid(3, 6, puzzle) &&
//          validateSubGrid(6, 0, puzzle) &&
//          validateSubGrid(6, 3, puzzle) &&
//          validateSubGrid(6, 6, puzzle);
//
//  }

    /**
   * Validates the 9 sub grids within the puzzle.
   *
   * @param puzzle
   * @return true if valid
   */
  public boolean validateSubGrids(Integer[][] puzzle) {
    int rowStart = 0;
    int colStart = 0;

    //traverse the sub grids...
    for (int x = 0, count = 1; x < 9; x++, count++){
      if (!validateSubGrid(rowStart, colStart, puzzle)) {
        return false;
      }
      if (count == 3) {// reset indexes every 3 grids
        count = 0;
        rowStart = rowStart + 3;
        colStart = 0;
      } else {
        colStart = colStart + 3;
      }
    }

    return true;

  }

  private boolean validateSubGrid(int rowStartIdx, int colStartIdx, Integer[][] puzzle) {
    List<Integer> values = new ArrayList<>();
    for (int row = rowStartIdx; row < rowStartIdx + 3; row++) {
      for (int col = colStartIdx; col < colStartIdx + 3; col++) {
        values.add(puzzle[row][col]);
      }
    }

    return isSequenceValid(values.stream());
  }

  /**
   * Determines if the given stream of Integers is valid.
   * Stream must contain 9 numbers, includes every number from 1-9, with no duplicates.
   *
   * @param stream
   * @return true if valid
   */
  private boolean isSequenceValid(Stream<Integer> stream) {
    return stream
        .filter(integer -> integer > 0 && integer < 10)
        .distinct()
        .count() == 9;
  }

}
