package kds;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class CsvReader {

  /**
   * Reads a CSV file representing a 9x9 sudoku grid.
   *
   * @param fileName
   * @return array representing a 9x9 sudoku grid
   * @throws IOException
   */
  public Integer[][] readFile(String fileName) throws IOException {
    List<List<Integer>> records = new ArrayList<>();

    try (BufferedReader br = new BufferedReader(new InputStreamReader(locateFile(fileName)))) {
      String line;
      while ((line = br.readLine()) != null) {
        if (line.trim().length() > 0) { //account for possible blank line at end of read
          String[] values = line.split(",");
          List<Integer> row =
              Arrays.stream(values)
                  .map(Integer::valueOf)
                  .collect(Collectors.toList());
          records.add(row);
        }
      }
    }

    return records.stream()
        .map(x -> x.toArray(new Integer[x.size()]))
        .toArray(Integer[][]::new);
  }

  private InputStream locateFile(String fileName) {
//      return getClass().getResourceAsStream(fileName);
      return ClassLoader.getSystemResourceAsStream(fileName);

  }

}
